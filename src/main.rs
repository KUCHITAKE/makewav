use std::{fs::File, path::Path};
use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
struct Args {
    /// input csv file  eg:0000_acc.csv
    #[clap(short, long, value_parser)]
    input: String,

    /// output wav file eg:acc.wav
    #[clap(short, long, value_parser)]
    output: String,

    /// sampling rate eg: 20_000(mic) 42_000(acc) 5_500(adxl356)
    #[clap(short, long, value_parser)]
    sampling_rate: u32,
}

fn main() -> std::io::Result<()> {
    let args = Args::parse();

    let mut rdr = csv::Reader::from_path(Path::new(args.input.as_str()))?;

    let data = rdr
        .records()
        .filter_map(|r| {
            r.ok()
                .and_then(|r| r.get(1).and_then(|r| r.parse::<i16>().ok()))
        })
        .collect();
    let data = wav::BitDepth::Sixteen(data);

    let mut wavfile = File::create(Path::new(args.output.as_str()))?;
    let header = wav::Header::new(wav::WAV_FORMAT_PCM, 1, args.sampling_rate, 16);
    wav::write(header, &data, &mut wavfile)?;

    Result::Ok(())
}
